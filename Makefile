.PHONY: all rst docs

all: docs

rst:
	# Create rst file from source

docs: rst
	# make -C docs/ singlehtml
	make -C docs/ html
