docs-template
=============

This is a template for documentation or technical documents based on Sphinx.

```sh
python -m venv env
. env/bin/activate
pip install -r requirements.txt
make
```

If all goes well, you'll find your html document in `docs/html` (which you can open locally with your browser).

See the document online: https://coslo.frama.io/template-docs/tutorial/

Authors
-------
Daniele Coslovich: https://www.units.it/daniele.coslovich/
